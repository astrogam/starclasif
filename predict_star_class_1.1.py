import pickle
import numpy as np
import os
from tensorflow import keras
import argparse
import sys
import csv
from pprint import pprint
import collections
from sklearn.metrics import fbeta_score

CLASS = "CLASS"
PREDICTIONS_FILE = 'predictions.csv'
DEFAULT_ML = '2'
ALL_ML = '7'

ML = {'1': 'Nearest Neighbors', '2': 'Linear SVM', '3': 'RBF SVM', '4': 'Gaussian Process', '5': 'Random Forest', '6': 'Neural Net'}
# We work with pickle models (*.model) and keras models (*.h5), so ML_name.model or ML_name.h5 should exist.

ML_plus_all = {'1': 'Nearest Neighbors', '2': 'Linear SVM', '3': 'RBF SVM', '4': 'Gaussian Process', '5': 'Random Forest', '6': 'Neural Net', ALL_ML: 'All'}
classes = {2: "B", 3: "A", 4: "F", 5: "G", 6: "K", 7: "M"}

def buildParser():
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", required=False,
        help="Path to CSV file with photometry of stars to predict class \
                IMPORTANT: Enter CSV file with headers, should have Name,B,V,J,H,K headers \
              Example: raw_set.csv \
              Results: " + PREDICTIONS_FILE)
    ap.add_argument("-p", "--photometry", required=False,
        help="Photometry of star to predict class in format B,V,J,H,K (No space separation)")
    ap.add_argument("-ml", "--machine-learning", required=False,
            help="Machine Learning model to make predictions: " + str(ML_plus_all) + " Default: " + DEFAULT_ML + ': ' + ML_plus_all[DEFAULT_ML])
    return ap

def parseArgs(ap):
    args = vars(ap.parse_args())
    return args

ap = buildParser()
args = parseArgs(ap)

# Errors
if not args['file'] and not args['photometry']:
    print("Enter photometry (-p) or a CSV file (-f) \n")
    ap.print_help()
    sys.exit()

# Machine learning model
models = []
if not args['machine_learning']:
    models.append(ML[DEFAULT_ML])

elif args['machine_learning'] == ALL_ML:
    #models = [ML[m] for m in ML]
    models = [ML['2'], ML['6']]

else:
    models.append(ML[args['machine_learning']])

# Start working...
if args['file']:
    print("Processing file: " + args['file'])
    dataset = []
    with open(args['file'],'r') as fp:
        for sample in csv.DictReader(fp):
            dataset.append(sample)
    csv_columns = list(dataset[0].keys())
    for p in ["B","V","J","H","K"]:
        if p not in csv_columns:
            print("File does not contain " + p + " photometry column")
            ap.print_help()
            sys.exit()
    X = []
    samples = []
    known_classes = []
    for s in dataset:
        try:
            if s["B"] != "" and s["V"] != "" and s["J"] != "" and s["H"] != "" and s["K"] != "":
                input_vector = [float(s["B"]), float(s["V"]), float(s["J"]), float(s["H"]), float(s["K"])]
                X.append(input_vector)
                samples.append(s)
                if CLASS in s:
                    known_classes.append(s[CLASS])
        except Exception as e:
            #print("Error in sample: " + s["Name"])
            continue
    print("Dataset loaded. " + str(len(dataset)) + " stars")
    print("Photometry for " + str(len(X)) + " stars") 
    if known_classes:
        known = dict(collections.Counter(known_classes))
        known = {k: v for k, v in sorted(known.items(), key=lambda item: item[1], reverse=True)}
        total = 0
        for k, v in known.items():
            total += v
        known = {k: "{:.2f}".format(v/total*100) + '%' for k, v in known.items()}
        print("Dataset is already classified, the abundances are:")
        print(known)
    for m in models:
        print("Predicting through model " + m)
        if os.path.isfile(m + '.model'):
            with open(m + '.model', 'rb') as fp:
                clf = pickle.load(fp)
            prediction = clf.predict(X)
            prediction_classes = [classes[p] for p in prediction]
        elif os.path.isfile(m + '.h5'):
            clf = keras.models.load_model(m + '.h5')
            prediction = clf.predict(X)
            prediction_classes = []
            for p in prediction:
                max_prob = p.max()
                i = np.where(p == max_prob)
                prediction_classes.append(classes[i[0][0]+1])
        # Some stats
        d = dict(collections.Counter(prediction_classes))
        d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}
        total = 0
        for k, v in d.items():
            total += v
        d = {k: "{:.2f}".format(v/total*100) + '%' for k, v in d.items()}
        print("Stats: " + str(d))
        if known_classes:
            success = 0
            for k, p in zip(known_classes, prediction_classes):
                if k == p:
                    success += 1
            print("Score: " + str(success/len(prediction_classes)))
            fbeta = fbeta_score(known_classes, prediction_classes, average=None, beta=2, labels=['B', 'A', 'F', 'G', 'K', 'M'])
            print("F(beta=2) Score [ B A F G K M ]: " + str(["{:.2f}".format(f) for f in fbeta]))
            fbeta = fbeta_score(known_classes, prediction_classes, average=None, beta=0.5, labels=['B', 'A', 'F', 'G', 'K', 'M'])
            print("F(beta=0.5) Score [ B A F G K M ]: " + str(["{:.2f}".format(f) for f in fbeta]))
            #fbeta = fbeta_score(known_classes, prediction_classes, average='micro', beta=2)
            #print("F(beta=0.5) Global score avg : " + str(fbeta))
        csv_columns.append(m)
        classified_dataset = dataset
        try:
            for sample, star_class in zip(samples, prediction_classes):
                the_index = classified_dataset.index(sample)
                classified_dataset[the_index][m] = star_class
        except IOError:
            print(str(e))
    print("Saving results to " + PREDICTIONS_FILE)
    csv_columns.append("Cluster")
    with open(PREDICTIONS_FILE, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for item in classified_dataset:
            if item['B'] != '':
                item['Cluster'] = os.path.basename(args['file'])
                #print(item['teff_val'])
                #print(item['Linear SVM'])
                writer.writerow(item)

elif args['photometry']:
    photometry = args["photometry"].split(',')
    input_vector = [float(p) for p in photometry]
    X = [input_vector]

    for m in models:
        if os.path.isfile(m + '.model'):
            with open(m + '.model', 'rb') as fp:
                clf = pickle.load(fp)
            prediction = clf.predict(X)
            for p in prediction:
                print(m + ': ' + classes[p])
        elif os.path.isfile(m + '.h5'):
            clf = keras.models.load_model(m + '.h5')
            prediction = clf.predict(X)[0]
            max_prob = prediction.max()
            i = np.where(prediction == max_prob)
            print(m + ': ' + classes[i[0][0]+1])

