from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord
import math
import os
import csv

def get_bvjhk_from_Vizier(ra, dec):
    r = {}
    center = coord.SkyCoord(ra=ra, dec=dec, unit=(u.deg, u.deg))
    # Query 2 MASS
    result = Vizier.query_region(center, radius=5*u.arcsec, catalog='II/246')
    if (result):
        table = result[0]
        distances = []
        for row in table:
            d = math.sqrt((row['RAJ2000'] - ra) ** 2 + (row['DEJ2000'] - dec) ** 2)
            distances.append(d)
        table.add_column(distances, name="distance")
        table.sort('distance')
        r['J'] = table[0]['Jmag']
        r['H'] = table[0]['Hmag']
        r['K'] = table[0]['Kmag']

    # Query APASS
    result = Vizier.query_region(center, radius=10*u.arcsec, catalog='II/336')
    if (result):
        table = result[0]
        distances = []
        for row in table:
            d = math.sqrt((row['RAJ2000'] - ra) ** 2 + (row['DEJ2000'] - dec) ** 2)
            distances.append(d)
        table.add_column(distances, name="distance")
        table.sort('distance')
        r['B'] = table[0]['Bmag']
        r['V'] = table[0]['Vmag']
    return r

folder = '../../Membership/OC_members'

for f in os.listdir(folder):
    print(f)
    if '.csv' not in f:
        print("Skipped!")
        continue
    filepath = os.path.join(folder, f)
    dataset = []
    with open(filepath,'r') as fp:
        for sample in csv.DictReader(fp):
            dataset.append(sample)
    csv_columns = list(dataset[0].keys())
    for s in dataset:
        for band in ['B','V','J','H','K']:
            s[band] = ''
        print(s['ra'])
        print(s['dec'])
        phot = get_bvjhk_from_Vizier(float(s['ra']), float(s['dec']))
        for band in phot:
            s[band] = phot[band]
    csv_columns = list(dataset[0].keys())
    with open(filepath, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for item in dataset:
            writer.writerow(item)

