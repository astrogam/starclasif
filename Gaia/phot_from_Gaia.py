import sympy
import sys
import csv
import os
from pprint import pprint

def b_v_from_g_v(g_v):
    x = sympy.Symbol('x', real=True)
    r = sympy.solve(-g_v -0.03704 -0.3915*x + 0.01855*(x**2)-0.03239*(x**3), x)
    return r[0]

def g_v_from_bp_rp(bp_rp):
    x = bp_rp
    r = -0.01968 -0.2344*x -0.12*(x**2) + 0.01490*(x**3)
    return r

def g_k_from_bp_rp(bp_rp):
    x = bp_rp
    r = -0.01885 +2.092*x -0.1345*(x**2)
    return r

def g_h_from_bp_rp(bp_rp):
    x = bp_rp
    r = -0.1621 +1.968*x -0.1328*(x**2)
    return r

def g_j_from_bp_rp(bp_rp):
    x = bp_rp
    r = 0.05255 +1.211*x +0.04797*(x**2) + -0.024367*(x**3)
    return r

def get_bvjhk_from_Gaia(g, bp, rp):
    bp_rp = bp - rp
    g_v = g_v_from_bp_rp(bp_rp)
    V = g - g_v
    b_v = b_v_from_g_v(g_v)
    B = b_v + V

    J = g - g_j_from_bp_rp(bp_rp)
    H = g - g_h_from_bp_rp(bp_rp)
    K = g - g_k_from_bp_rp(bp_rp)

    r = {'B': round(B,2),
         'V': round(V,2),
         'J': round(J,2),
         'H': round(H,2),
         'K': round(K,2)}
    return r

gh = gaia_phot_headers = { 'G': 'phot_g_mean_mag', 
                      'Bp': 'phot_bp_mean_mag', 
                      'Rp': 'phot_rp_mean_mag'}

folder = '../../Membership/OC_members'

for f in os.listdir(folder):
    print(f)
    if '.csv' not in f:
        print("Skipped!")
        continue
    filepath = os.path.join(folder, f)
    dataset = []
    with open(filepath,'r') as fp:
        for sample in csv.DictReader(fp):
            dataset.append(sample)
    csv_columns = list(dataset[0].keys())
    for band in gh:
        if gh[band] not in csv_columns:
            print("No photometric data!")
            sys.exit()
    for s in dataset:
        if float(s[gh['G']]) <= 13:
            johnson_phot = get_bvjhk_from_Gaia(float(s[gh['G']]), float(s[gh['Bp']]), float(s[gh['Rp']]))
            for band in johnson_phot:
                s[band] = johnson_phot[band]
        else:
            for band in ['B','V','J','H','K']:
                s[band] = ''
    csv_columns = list(dataset[0].keys())
    with open(filepath, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for item in dataset:
            writer.writerow(item)

